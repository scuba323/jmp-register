= jmp-register =

The registration wizard for JMP.  This wizard requires sgx-catapult and
jmp-fwdcalls to operate.  It also requires a PayPal premier/merchant account.

The project should run on any web server configured to serve PHP (and to show
index.php and index.xhtml as default directory pages) that has phpredis (ie. the
php5-redis package in Debian).  It also requires Ruby and a properly-configured
register4/.htaccess file (that will run config.ru, ie. using CGI or Passenger).

jmp-register requires that an instance of Cheogram be setup, as it assumes that
you will want users to be able to use Cheogram JIDs for their contacts (ie.
+12113114111@cheogram.com instead of +12113114111@example.sgx.soprani.ca).  You
must setup a separate JID for registering users with Cheogram (via <forwarded/>)
and configure Cheogram to accept that JID.  If you don't want to use Cheogram,
you will need to replace that code in register[34] before running jmp-register.

A SIP domain at Bandwidth AP must be configured before using jmp-register.  This
is used by jmp-register through the $catapult_domain_* variables specified
below.  The prefix/"Domain" is the "example" part of "example.bwapp.bwsip.io".

Two PayPal subscription buttons must be setup and the details configured below.
jmp-register is configured by default to use the PayPal Sandbox - in order to
run jmp-register in production you must change "sandbox.paypal" to "paypal" (in
register6/index.php and register7/index.php ).

As-written, jmp-register uses MaxMind's GeoIP2 Precision Web Services.  Please
ensure you have an account configured below, or disable the code before running.

jmp-register relies on https://github.com/singpolyma/mnemonicode for generating
easy-to-remember passwords.  Please compile the "mnencode" binary and place it
at register4/mnencode .

There are two settings files for jmp-register, named fancynum-jmp.php and
settings-jmp.php - the first is used only for specifying special/fancy numbers
to show users through the number suggester, but must be present even if unused.
The fancynum-jmp.php file must be placed at ../../../../fancynum-jmp.php
(relative to register3/index.php for example) and it must contain at least the
following (the default values below effectively disable special number feature):

<?php
$fancy_max = 10;			# amount to display for area code search
$fancy_area_code_neighbours = array();	# area codes to show number by (all =>0)
$fancynums = array(array());		# inner array lists nums in acct to show
?>

Note that a settings file must be placed at ../../../../settings-jmp.php
(relative to register3/index.php for example).  The file must define these
variables:

* $support_number (an sgx-catapult hosted E.164 number to send from)
* $notify_receiver_email (the email address where notification list requests go)
* $fwdcalls_url (should match the <delivery_receipt_url> that jmp-fwdcalls uses)
* $sgx_jid (the JID that sgx-catapult uses, ie. "example.sgx.soprani.ca")
* $cheogram_did (the DID that Cheogram uses, ie. "+12266669977")
* $cheogram_jid (the JID that Cheogram uses, ie. "cheogram.com")
* $cheogram_register_jid (the JID that Cheogram trusts for registering users)
* $cheogram_register_token (the password for $cheogram_register_jid)
* $register_base_url (the "http://a.b/c" in "http://a.b/c/register3/index.php")
* $porting_form_url (URL that creates a porting form using porting3 parameters)
* $paypal_pdt_token (your PayPal "PDT identity token" - see settings pages)
* $paypal_tags_monthly (code for monthly PayPal button minus the <form>/</form>)
* $paypal_tags_annual (code for annual PayPal button minus the <form>/</form>)
* $paypal_tags_one_year (code for 1-year PayPal button minus the <form>/</form>)
* $redis_host (should match what sgx-catapult uses)
* $redis_port (should match what sgx-catapult uses)
* $redis_auth (should match what sgx-catapult uses; empty string if no auth)
* $key_ttl_seconds (recommended: 3600)
* $mm_user (MaxMind "User ID", for GeoIP2 Precision Web Services)
* $mm_token (MaxMind "License key", for GeoIP2 Precision Web Services)
* $user (the user ID of the Bandwidth AP user being used; starts with "u-")
* $tuser (the Bandwidth AP "Api Token" that sgx-catapult uses; starts with "t-")
* $token (the Bandwidth AP "Api Secret" that sgx-catapult uses)
* $catapult_application_id (sgx-catapult Bandwidth AP app ID; starts with "a-")
* $catapult_domain_id (sgx-catapult Bandwidth AP domain ID; starts with "rd-")
* $catapult_domain_prefix (sgx-catapult Bandwidth AP "Domain Name" of endpoints)


Copyright (C) 2017  Denver Gingerich <denver@ossguy.com>

Copying and distribution of this README.creole file, with or without
modification, are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
